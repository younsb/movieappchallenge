# Movie APP CHALLENGE REPO EXPO

THIS IS AN REACT NATIVE APP BUILD USING EXPO

CMD LIKE `REACT-NATIVE ...` are no longer available

Please refer to expo official doc for more info => **[expo](https://docs.expo.io/versions/latest/index.html)**.

### Necessary steps to get your application up and running.

---

### set up?

- Summary of set up

      	git clone git clone https://younsb@bitbucket.org/younsb/movieappchallenge.git

      	yarn (recomanded) or npm install

* Configuration

      	* EXPO SDK

* Code Editor

      	* VS CODE

* Global Dependencies

      	- Yarn.
      	- ESLINT.

* Deployment instructions

      	yarn start

* Testing instructions

      	yarn test

### VS CODE CONFIG

Install these extensions.

- Prettier - JavaScript formatter (required)

- ESLint (required)

- Rainbow Brackets

- Color Highlight

- Auto Complete Tag

- Auto Rename Tag

- ES7 React/Redux/GraphQL/React-Native snippets

### Themoviedb API

- To add your own api key please edit the environment file

```js
// src/environment/environment.js
```

Please refer to themoviedb official doc for more info => **[API DOCS](https://developers.themoviedb.org/3/getting-started)**.

### Summary



![screen1](screens/screenshot_1566742903.png =200x400) ![screen2](screens/screenshot_1566742927.png =200x400)


### Contribution guidelines

- Alwayse run the app before commit`
- Code review
