import React from 'react';
import PercentageCircle from 'react-native-percentage-circle';
import { Caption, Title, View } from '@shoutem/ui';

import styles from './userScoreStyles';

function UserScore({ score: { vote_average, vote_count } }) {
  const userScore = vote_average * 10;

  if (!userScore) return null;
  return (
    <View>
      <PercentageCircle
        radius={35}
        borderWidth={6}
        percent={userScore}
        color="#d2d531"
        bgcolor="rgba(0,0,0,0.6)"
        innerColor="rgba(0,0,0,1)"
      >
        <Title style={styles.userScorePrecentText}>{userScore}</Title>
      </PercentageCircle>
      <Caption style={styles.userScoreText}>{`User Score (${vote_count})`}</Caption>
    </View>
  );
}

export default UserScore;
