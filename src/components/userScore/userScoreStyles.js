import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  userScoreText: {
    color: '#fff',
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
  },
  userScorePrecentText: { color: '#fff', fontSize: 15 },
});
