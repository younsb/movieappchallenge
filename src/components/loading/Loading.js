import React from 'react';
import { ActivityIndicator, View } from 'react-native';
import styles from './lodingStyles';
import colors from '../../constants/Colors';

const Loading = ({ color = colors.primary, size = 'large', center, animating = true, padding }) => {
  const { container, fullCenter } = styles;

  const containerStyles = [container];
  if (center) {
    containerStyles.push(fullCenter);
  }
  if (padding) {
    containerStyles.push({ paddingVertical: padding });
  }
  return (
    <View>
      {animating ? (
        <View style={containerStyles}>
          <ActivityIndicator animating={animating} size={size} color={color} />
        </View>
      ) : null}
    </View>
  );
};

export default Loading;
