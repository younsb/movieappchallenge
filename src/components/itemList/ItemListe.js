import React, { memo } from 'react';
import { Subtitle, Row, Caption, Image, View, TouchableOpacity } from '@shoutem/ui';
import { withNavigation } from 'react-navigation';

import environment from '../../environment/environment';

const ItemList = ({ navigation, data: { item } }) => {
  const { title, overview, id, poster_path, release_date } = item;
  return (
    <View style={{ padding: 10 }}>
      <View
        style={{
          shadowOffset: { width: 0, height: 0 },
          shadowRadius: 3,
          shadowColor: 'rgba(0,0,0,.2)',
          shadowOpacity: 0.3,
        }}
      >
        <TouchableOpacity
          style={{
            elevation: 2,
            borderRadius: 10,
            overflow: 'hidden',
          }}
          onPress={() => navigation.navigate('MovieDetail', { movieId: id })}
        >
          <Row>
            <Image
              style={{ width: 100, height: 140 }}
              resizeMode="contain"
              source={{ uri: `${environment.IMAGE_ENDPONT}/t/p/w154${poster_path}` }}
            />
            <View styleName="vertical stretch space-between">
              <View>
                <Subtitle numberOfLines={2}>{title}</Subtitle>
                <Caption numberOfLines={3} style={{ color: 'black' }}>
                  {overview}
                </Caption>
              </View>
              <View styleName="horizontal">
                <Caption>{release_date}</Caption>
              </View>
            </View>
          </Row>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default memo(withNavigation(ItemList));
