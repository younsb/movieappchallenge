import React from 'react';
import Loading from '../loading/Loading';

const ListFooter = ({ isLoading }) => {
  if (!isLoading) return null;
  return <Loading size="large" padding={20} />;
};

export default ListFooter;
