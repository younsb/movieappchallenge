import React from 'react';
import { createIconSetFromFontello } from '@expo/vector-icons';

import fontelloConfig from '../../assets/fonts/config.json';

const CustomIcon = createIconSetFromFontello(fontelloConfig, 'CustomIcon');

const Icon = ({ style, name }) => <CustomIcon name={name} style={style} />;

export default Icon;
