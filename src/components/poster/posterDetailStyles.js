import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: { backgroundColor: 'rgba(0,0,0,0.15)' },
  subPosterContainer: {
    flexDirection: 'row',
    padding: 20,
    paddingTop: 10,
    paddingBottom: 0,
    paddingRight: 0,
    alignItems: 'flex-end',
  },
  subPoster: {
    width: '30%',
    height: 180,
    position: 'absolute',
    left: 20,
  },
  subPosterImage: {
    width: '100%',
    height: 180,
  },
  titleContainer: {
    padding: 15,
    paddingTop: 0,
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: '66%',
    marginLeft: '30%',
    alignItems: 'center',
  },
});
