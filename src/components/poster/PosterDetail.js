import React from 'react';
import { View, Image } from 'react-native';
import { ImageBackground, Caption, Title, Lightbox } from '@shoutem/ui';

import { formatDate } from '../../utils';
import environment from '../../environment/environment';
import UserScore from '../userScore/UserScore';
import styles from './posterDetailStyles';

const PosterDetail = ({ title, posterPath, releaseDate, backdropPath, score }) => {
  const { IMAGE_ENDPONT } = environment;
  return (
    <View style={styles.container}>
      <ImageBackground
        styleName="large"
        source={{
          uri: `${IMAGE_ENDPONT}/t/p/w500${backdropPath}`,
        }}
      />
      <View style={{ position: 'absolute', top: 180, right: 30 }}>
        <UserScore score={score} />
      </View>
      <View style={styles.subPosterContainer}>
        <View style={styles.subPoster}>
          <Lightbox
            style={{ flex: 1, alignItem: 'stretch' }}
            renderContent={() => (
              <Image
                resizeMode="contain"
                source={{
                  uri: `${IMAGE_ENDPONT}/t/p/w780${posterPath}`,
                }}
                style={{ width: '100%', height: 700 }}
              />
            )}
          >
            <Image
              style={styles.subPosterImage}
              resizeMode="contain"
              source={{
                uri: `${IMAGE_ENDPONT}/t/p/w500${posterPath}`,
              }}
            />
          </Lightbox>
        </View>
        <View style={styles.titleContainer}>
          <Title>{title}</Title>
          <Caption>{`(${formatDate(releaseDate, 'year')})`}</Caption>
        </View>
      </View>
    </View>
  );
};

export default PosterDetail;
