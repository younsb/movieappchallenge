/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { View, TouchableWithoutFeedback, Keyboard } from 'react-native';

const Container = ({ children, ...ViewProps }) => (
  <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
    <View {...ViewProps}>{children}</View>
  </TouchableWithoutFeedback>
);

export default Container;
