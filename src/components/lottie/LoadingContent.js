import React from 'react';
import { View } from 'react-native';
import LottieView from 'lottie-react-native';

const LoadingContent = () => (
  <View
    style={{
      padding: 0,
      justifyContent: 'flex-start',
      alignContent: 'flex-start',
      alignItems: 'flex-start',
      width: '100%',
      height: 200,
    }}
  >
    <LottieView
      resizeMode="cover"
      source={require('../../assets/lottiefiles/loading-animation.json')}
      autoPlay
      loop
    />
  </View>
);

export default LoadingContent;
