import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    position: 'absolute',
    width: '100%',
    bottom: -20,
  },
  box: {
    width: '100%',
    backgroundColor: '#f44336',
  },
  buttonWrapper: {
    position: 'absolute',
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    top: -30,
    right: 20,
  },
  button: {
    backgroundColor: '#FFF',
    borderRadius: 20,
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 4,
    borderColor: 'rgba(0, 0,0, 0.1)',
    borderWidth: 1,
  },
  buttonText: {
    color: 'grey',
    fontWeight: 'bold',
  },
  textWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 20,
  },
  text: {
    color: 'rgba(255, 255, 255, 0.8)',
    fontWeight: 'bold',
  },
});
