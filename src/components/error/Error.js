import React, { useState, useRef, useEffect } from 'react';
import { LayoutAnimation, Text, TouchableOpacity, View, Animated } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';

import styles from './ErrorStyles';
import { errorActions } from '../../redux/actions/errorAction';

function Error() {
  const [h] = useState(new Animated.Value(0));
  const isVisible = useSelector((state) => state.error.isVisible);
  const dispatch = useDispatch();

  const prevIsVisibleRef = usePrevious({ isVisible });
  useEffect(() => {
    if (prevIsVisibleRef && prevIsVisibleRef.isVisible !== isVisible) {
      toogleAnimation({ isVisible, h });
    }
  }, [isVisible]);

  return (
    <View style={styles.container}>
      <Animated.View style={[styles.box, { height: h }]}>
        <View style={styles.textWrapper}>
          <Text style={styles.text}>Une erreur est survenue. merci d'essayer à nouveau</Text>
        </View>
      </Animated.View>
      <View style={styles.buttonWrapper}>
        <TouchableOpacity onPress={() => onPressClose(dispatch)} style={{ padding: 10 }}>
          <View style={styles.button}>
            <Text style={styles.buttonText}>X</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const toogleAnimation = ({ isVisible, h }) => {
  LayoutAnimation.linear();
  const toValue = isVisible ? 110 : 0;
  Animated.timing(h, {
    toValue,
    duration: 400,
  }).start();
};

const usePrevious = (value) => {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
};

const onPressClose = (dispatch) => {
  dispatch(errorActions.toogleError());
};

export default Error;
