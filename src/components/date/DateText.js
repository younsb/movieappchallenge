/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { Text } from '@shoutem/ui';
import { formatDate } from '../../utils';

const DateText = ({ date, format = 'date', ...otherProps }) => (
  <Text {...otherProps}>{formatDate(date, format)}</Text>
);

export default DateText;
