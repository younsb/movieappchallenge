import React from 'react';
import { ScrollView, View } from 'react-native';
import { Text, Button } from '@shoutem/ui';

import styles from './genresStyles';

const Badge = ({ data = [] }) => (
  <ScrollView style={styles.container} horizontal>
    <View style={{ flexDirection: 'row' }}>
      {data.map(({ name, id }) => (
        <Button
          key={id}
          styleName="secondary"
          style={{ borderRadius: 30, margin: 3, marginTop: 0 }}
        >
          <Text>{name}</Text>
        </Button>
      ))}
    </View>
  </ScrollView>
);

export default Badge;
