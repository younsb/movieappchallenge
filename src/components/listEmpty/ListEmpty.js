import React from 'react';
import { Row, View, Subtitle, Icon } from '@shoutem/ui';

import colors from '../../constants/Colors';

const ListEmpty = ({ isLoading }) => {
  if (isLoading) return null;
  return (
    <View style={{ padding: 30, paddingTop: 40 }}>
      <Row style={{ borderWidth: 1, borderColor: '#f5f2f0' }}>
        <Icon name="about" style={{ color: colors.primary }} />
        <View styleName="stretch">
          <Subtitle>There are no movies that matched your query.</Subtitle>
        </View>
      </Row>
    </View>
  );
};

export default ListEmpty;
