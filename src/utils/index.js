import moment from 'moment';

/* eslint-disable implicit-arrow-linebreak */
export const objectToparamUrl = (objParams) => {
  if (!objParams || typeof objParams !== 'object') return '';
  const params = Object.keys(objParams)
    .map((key) => `${key}=${encodeURIComponent(objParams[key])}`)
    .join('&');
  if (params) return `&${params}`;
  return '';
};

export const delay = (ms) => new Promise((res) => setTimeout(res, ms));

export const formatDate = (date, format) => {
  const dateFormat = {
    date: 'D MMMM YYYY',
    year: 'YYYY',
    time: 'h[H]mm',
  };
  if (!date || !moment(date).isValid()) return '';
  const outputFormat = format ? dateFormat[format] : dateFormat.date;
  const data = moment(date).format(outputFormat);

  return data;
};
