import movieActionTypes from '../types/movieTypes';

const getMovies = (filter) => ({
  type: movieActionTypes.MOVIES_GET_REQUEST,
  payload: filter,
});

const getMoviesSuccess = (data) => ({
  type: movieActionTypes.MOVIES_GET_SUCCESS,
  payload: data,
});
const getMoviesFailure = (errors) => ({
  type: movieActionTypes.MOVIES_GET_FAILURE,
  errors,
});

const getMovieById = (id) => ({
  type: movieActionTypes.MOVIE_GET_REQUEST,
  payload: id,
});

const getMovieByIdSuccess = (data) => ({
  type: movieActionTypes.MOVIE_GET_SUCCESS,
  payload: data,
});
const getMovieByIdFailure = (errors) => ({
  type: movieActionTypes.MOVIE_GET_FAILURE,
  errors,
});

// eslint-disable-next-line import/prefer-default-export
export const movieActions = {
  getMovies,
  getMoviesSuccess,
  getMoviesFailure,
  getMovieById,
  getMovieByIdSuccess,
  getMovieByIdFailure,
};
