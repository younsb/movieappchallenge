import errorActionTypes from '../types/errorTypes';

const toogleError = (isVisible = null) => ({
  type: errorActionTypes.ERROR_TOOGLE,
  payload: isVisible,
});

// eslint-disable-next-line import/prefer-default-export
export const errorActions = {
  toogleError,
};
