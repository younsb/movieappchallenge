import movieActionTypes from '../types/movieTypes';

const INITIAL_STATE = {
  movies: {
    list: [],
    totalPages: 0,
    refreching: false,
    filter: {
      query: '',
      page: 1,
    },
  },
  isLoading: true,
  movieDetails: { item: {} },
};

const movieReducer = (state = INITIAL_STATE, action = {}) => {
  switch (action.type) {
    case movieActionTypes.MOVIES_GET_REQUEST: {
      const { page, refreching } = action.payload;
      const list = page === 1 && !refreching ? [] : state.movies.list;
      return {
        ...state,
        movies: {
          ...state.movies,
          list,
          refreching: false,
          filter: action.payload,
        },
        isLoading: true,
      };
    }
    case movieActionTypes.MOVIES_GET_SUCCESS: {
      const { results, total_pages, page } = action.payload;
      const list = page === 1 ? [] : state.movies.list;

      return {
        ...state,
        movies: {
          list: list.concat(results),
          totalPages: total_pages,
          filter: {
            ...state.movies.filter,
            page,
          },
        },
        isLoading: false,
      };
    }

    case movieActionTypes.MOVIE_GET_REQUEST:
      return {
        ...state,
        isLoading: true,
        movieDetails: { item: state.movies.list.find((item) => item.id === action.payload) },
      };
    case movieActionTypes.MOVIE_GET_SUCCESS:
      return {
        ...state,
        isLoading: false,
        movieDetails: { item: action.payload },
      };
    default:
      return state;
  }
};

export default movieReducer;
