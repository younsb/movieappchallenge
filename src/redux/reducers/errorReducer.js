import errorActionTypes from '../types/errorTypes';

const INITIAL_STATE = {
  isVisible: false,
};

const errorReducer = (state = INITIAL_STATE, action = {}) => {
  switch (action.type) {
    case errorActionTypes.ERROR_TOOGLE: {
      return {
        ...state,
        isVisible: action.payload !== null ? action.payload : !state.isVisible,
      };
    }
    default:
      return state;
  }
};

export default errorReducer;
