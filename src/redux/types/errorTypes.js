const errorActionTypes = {
  ERROR_TOOGLE: 'ERROR_TOOGLE',
  ERROR_SHOW: 'ERROR_SHOW',
  ERROR_HIDE: 'ERROR_HIDE',
};

export default errorActionTypes;
