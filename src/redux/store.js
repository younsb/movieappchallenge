import { createStore, applyMiddleware } from 'redux';
import { YellowBox } from 'react-native';
import logger from 'redux-logger';
import createSagaMiddleware from 'redux-saga';

import rootReducer from './reducers';
import rootSaga from './sagas';

const sagaMiddleware = createSagaMiddleware();
const middleware = [sagaMiddleware];

// eslint-disable-next-line no-undef
if (__DEV__) {
  // eslint-disable-next-line no-undef
  YellowBox.ignoreWarnings([
    'Require cycle:',
    'ReactNative.NativeModules.LottieAnimationView.getConstants',
  ]);
  middleware.push(logger);
}
const store = createStore(rootReducer, applyMiddleware(...middleware));

export default store;
sagaMiddleware.run(rootSaga);
