import { call, put, takeLatest } from 'redux-saga/effects';

import movieTypes from '../types/movieTypes';
import { objectToparamUrl } from '../../utils';
import { getPopularMoviesApi, getMoviesApi, getMovieByIdApi } from './providers';
import errorActionTypes from '../types/errorTypes';

function* fetchMovies(action) {
  try {
    const params = objectToparamUrl(action.payload);
    let res = {};
    if (action.payload.query) {
      res = yield call(getMoviesApi, params);
    } else {
      res = yield call(getPopularMoviesApi, params);
    }
    const results = yield res.data;
    yield put({ type: movieTypes.MOVIES_GET_SUCCESS, payload: results });
  } catch (e) {
    yield put({ type: movieTypes.MOVIES_GET_FAILURE, message: e });
    yield put({ type: errorActionTypes.ERROR_TOOGLE, payload: true });
  }
}

function* fetchMovieById(action) {
  try {
    const res = yield call(getMovieByIdApi, action.payload);
    const results = yield res.data;
    yield put({ type: movieTypes.MOVIE_GET_SUCCESS, payload: results });
  } catch (e) {
    yield put({ type: movieTypes.MOVIE_GET_FAILURE, message: e });
    yield put({ type: errorActionTypes.ERROR_TOOGLE, payload: true });
  }
}

function* watchMovieRequest() {
  yield takeLatest(movieTypes.MOVIES_GET_REQUEST, fetchMovies);
  yield takeLatest(movieTypes.MOVIE_GET_REQUEST, fetchMovieById);
}

export default watchMovieRequest;
