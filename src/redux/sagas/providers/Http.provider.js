import axios from 'axios';
import environment from '../../../environment/environment';

const { ENDPONT, API_KEY, FETCH_TIMEOUT } = environment;

axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.headers.put['Content-Type'] = 'application/json';
axios.defaults.timeout = FETCH_TIMEOUT;

class HttpProvider {
  static get(url, data = '') {
    return axios.get(`${ENDPONT}${url}?api_key=${API_KEY}${data}`);
  }

  static post(url, data) {
    return axios.post(`${ENDPONT}${url}?api_key${API_KEY}`, data);
  }

  static put(url, data) {
    return axios.put(`${ENDPONT}${url}?api_key${API_KEY}`, data);
  }
}
export default HttpProvider;
