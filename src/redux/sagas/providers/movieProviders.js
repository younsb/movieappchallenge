import http from './Http.provider';

export const getMoviesApi = (filter) => http.get('/search/movie', filter);
export const getPopularMoviesApi = (filter) => http.get('/movie/popular/', filter);
export const getMovieByIdApi = (id) => http.get(`/movie/${id}`);
