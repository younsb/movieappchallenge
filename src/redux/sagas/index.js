import { fork } from 'redux-saga/effects';

import watchMovieRequest from './movieSaga';

export default function* Root() {
  yield fork(watchMovieRequest);
}
