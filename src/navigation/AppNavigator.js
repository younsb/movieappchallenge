import { createAppContainer, createStackNavigator } from 'react-navigation';

import HomeScreen from '../screens/homeScreen/HomeScreen';
import MovieDetailScreen from '../screens/movieDetailScreen/MovieDetailScreen';

const StackNavigator = createStackNavigator(
  { Home: HomeScreen, MovieDetail: MovieDetailScreen },
  { initialRouteName: 'Home' }
);
export default createAppContainer(StackNavigator);
