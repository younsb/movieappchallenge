import React from 'react';
import { FlatList } from 'react-native';
import { TextInput, Subtitle, Image, View } from '@shoutem/ui';
import { connect } from 'react-redux';
import { debounce } from 'lodash';

import { movieActions } from '../../redux/actions';

import CustomIcon from '../../components/icon/CustomIcon';
import ItemList from '../../components/itemList/ItemListe';
import Container from '../../components/container/Container';
import ListFooter from '../../components/listFooter/ListFooter';
import ListEmpty from '../../components/listEmpty/ListEmpty';
import styles from './homeScreenStyles';

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.handleOnChangeSearch = debounce(this.handleOnChangeSearch.bind(this), 500);
  }

  componentDidMount() {
    const { getMovies, filter } = this.props;
    getMovies(filter);
  }

  handleRefresh = () => {
    const { getMovies, filter } = this.props;

    getMovies({ ...filter, page: 1, refrech: true });
  };

  handleEndReached = () => {
    const { getMovies, isLoading, filter, totalPages } = this.props;

    if (!isLoading && filter.page < totalPages) {
      getMovies({ ...filter, page: filter.page + 1 });
    }
  };

  handleOnChangeSearch(searchTerm) {
    const { getMovies, filter } = this.props;
    getMovies({ ...filter, page: 1, query: searchTerm });
  }

  scrollToTop() {
    this.scrollToOffset(0);
  }

  render() {
    const { movies, isLoading } = this.props;
    return (
      <Container style={styles.container}>
        <View style={styles.container}>
          <View style={styles.welcomeContainer}>
            <Image
              style={styles.welcomeImage}
              source={require('../../assets/images/welcomeHeader.jpg')}
            />
            <View style={styles.welcomMessage}>
              <CustomIcon name="logo" style={styles.logo} />
              <Subtitle style={styles.subtitle}>
                DISCOVER NEW MOVIES
                {'\n &'}
                TV SHOWS
              </Subtitle>
            </View>
          </View>

          <View style={styles.searchContainer}>
            <TextInput
              placeholder="Search for a movie, tv show, person..."
              style={styles.searchBar}
              onChangeText={(text) => this.handleOnChangeSearch(text)}
            />
            <FlatList
              style={styles.resultContainer}
              data={movies}
              refreshing={false}
              onRefresh={this.handleRefresh}
              keyExtractor={(movie) => `id_${movie.id}`}
              renderItem={(movie) => <ItemList data={movie} />}
              onEndReachedThreshold={2}
              ListEmptyComponent={<ListEmpty isLoading={isLoading} />}
              ListFooterComponent={<ListFooter isLoading={isLoading} />}
              onEndReached={this.handleEndReached}
            />
          </View>
        </View>
      </Container>
    );
  }
}

HomeScreen.navigationOptions = () => ({
  header: null,
  headerStyle: { backgroundColor: 'transparent' },
});

const mapStateToProps = ({
  movie: {
    movies: { list, filter, totalPages },
    isLoading,
  },
}) => {
  return { movies: list, isLoading, filter, totalPages };
};
export default connect(
  mapStateToProps,
  { getMovies: movieActions.getMovies }
)(HomeScreen);
