import { Platform, StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  welcomeContainer: {
    backgroundColor: 'black',
    marginBottom: 0,
    height: 200,
    overflow: 'hidden',
  },
  welcomeImage: {
    width: '100%',
    position: 'absolute',
    zIndex: 1,
  },
  welcomMessage: {
    zIndex: 3,
    alignItems: 'center',
    display: 'flex',
    flexGrow: 1,
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    marginTop: -15,
  },
  subtitle: {
    color: '#ffffff',
    textAlign: 'center',
    marginTop: 28,
    fontSize: 17,
  },
  searchContainer: {
    backgroundColor: '#ffffff',
    minHeight: 300,
    display: 'flex',
    flexGrow: 1,
  },
  searchBar: {
    borderRadius: 30,
    elevation: 20,
    shadowOffset: { width: 0, height: -3 },
    shadowRadius: 3,
    shadowColor: 'rgba(0,0,0,.5)',
    shadowOpacity: 1,
    marginTop: -30,
  },
  resultContainer: { display: 'flex', flex: 1 },
  logo: { color: '#ffffff', fontSize: 30 },
});
