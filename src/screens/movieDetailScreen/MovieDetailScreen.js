import React, { Component } from 'react';
import { ScrollView, View } from 'react-native';
import { Text, Title, Divider } from '@shoutem/ui';
import { connect } from 'react-redux';
import { get } from 'lodash';

import { movieActions } from '../../redux/actions';

import Genres from '../../components/genres/GenresList';
import PosterDetail from '../../components/poster/PosterDetail';
import styles from './movieDetailScreenStyles';
import LoadingContent from '../../components/lottie/LoadingContent';

class MovieDetailScreen extends Component {
  static navigationOptions = {
    title: '',
    headerTransparent: true,
    headerLeftContainerStyle: {
      elevation: 8,
      backgroundColor: 'rgba(0,0,0,0.4)',
      overflow: 'hidden',
      borderRadius: 30,
    },
    headerTintColor: '#fff',
  };

  componentDidMount() {
    const { navigation, getMovieById } = this.props;
    const movieId = get(navigation, 'state.params.movieId', null);

    if (!movieId) navigation.goBack();
    else getMovieById(movieId);
  }

  render() {
    const {
      isLoading,
      movie: {
        title,
        overview,
        poster_path,
        release_date,
        backdrop_path,
        genres,
        vote_average,
        vote_count,
      },
    } = this.props;
    return (
      <View style={styles.container}>
        <PosterDetail
          title={title}
          posterPath={poster_path}
          releaseDate={release_date}
          backdropPath={backdrop_path}
          score={{ vote_average, vote_count }}
        />
        {isLoading ? (
          <LoadingContent />
        ) : (
          <ScrollView style={styles.scrollView}>
            <Title styleName="md-gutter-bottom">Genres</Title>
            <Genres data={genres} />
            <Divider clear />
            <Title styleName="md-gutter-bottom">Overview</Title>
            <Text style={{ textAlign: 'justify' }}>{overview}</Text>
          </ScrollView>
        )}
      </View>
    );
  }
}
const mapStateToProps = ({
  movie: {
    movieDetails: { item },
    isLoading,
  },
}) => {
  return { movie: item, isLoading };
};
export default connect(
  mapStateToProps,
  { getMovieById: movieActions.getMovieById }
)(MovieDetailScreen);
