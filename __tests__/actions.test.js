import movieActionTypes from '../src/redux/types/movieTypes';
import { movieActions } from '../src/redux/actions';

describe('Search Movies action', () => {
  it('Sould return proper flter with the type', () => {
    const filter = { page: 1, query: 'the movie' };
    const expected = {
      type: movieActionTypes.MOVIES_GET_REQUEST,
      payload: filter,
    };
    expect(movieActions.getMovies(filter)).toEqual(expected);
  });

  it('Sould return data with the type success', () => {
    const data = {
      page: 1,
      total_results: 3,
      total_pages: 1,
      results: [
        {
          poster_path: '/rdQrh59Gq0DX8OmoQeECUtBkoH5.jpg',
          id: 169822,
          title: 'Fast and Furious',
          overview: 'Joel and Garda Sloan, a .....',
          release_date: '1939-10-06',
        },
      ],
    };
    const expected = {
      type: movieActionTypes.MOVIES_GET_SUCCESS,
      payload: data,
    };
    expect(movieActions.getMoviesSuccess(data)).toEqual(expected);
  });
});

describe('get movie by id action', () => {
  it('Sould return proper object with the movie ID', () => {
    const id = 34567890;
    const expected = {
      type: movieActionTypes.MOVIE_GET_REQUEST,
      payload: id,
    };
    expect(movieActions.getMovieById(id)).toEqual(expected);
  });

  it('Sould return data with the type success', () => {
    const data = {
      backdrop_path: '/hpgda6P9GutvdkDX5MUJ92QG9aj.jpg',
      genres: [{ id: 28, name: 'Action' }],
      id: 384018,
      overview:
        "A spinoff of The Fate of the Furious, focusing on Johnson's US Diplomatic Security Agent Luke Hobbs forming an unlikely alliance with Statham's Deckard Shaw.",
      poster_path: '/keym7MPn1icW1wWfzMnW3HeuzWU.jpg',
      release_date: '2019-08-01',
      title: 'Fast and Furious Presents: Hobbs and Shaw',
      vote_average: 6.5,
      vote_count: 780,
    };
    const expected = {
      type: movieActionTypes.MOVIE_GET_SUCCESS,
      payload: data,
    };
    expect(movieActions.getMovieByIdSuccess(data)).toEqual(expected);
  });
});
