import { formatDate, objectToparamUrl } from '../src/utils';

describe('date texting', () => {
  it('should return year date', () => {
    expect(formatDate('10-10-2020', 'year')).toBe('2020');
  });

  it('date should handle undefined', () => {
    expect(formatDate(undefined, 'year')).toBe('');
  });

  it('date should handle null', () => {
    expect(formatDate(null, 'year')).toBe('');
  });

  it('date should handle invalid dateFormat', () => {
    expect(formatDate('sqdqs')).toBe('');
  });
});

describe('object To param Url', () => {
  it('should return a valid url Params', () => {
    expect(objectToparamUrl({ page: 1, query: 'the movie' })).toBe('&page=1&query=the%20movie');
    expect(objectToparamUrl({ page: 1 })).toBe('&page=1');
  });

  it('date should handle empty object', () => {
    expect(objectToparamUrl({})).toBe('');
  });

  it('date should handle null', () => {
    expect(objectToparamUrl(null)).toBe('');
  });
});
