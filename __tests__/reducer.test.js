import movieActionTypes from '../src/redux/types/movieTypes';
import movieReducer from '../src/redux/reducers/movieReducer';

const INITIAL_STATE = {
  movies: {
    list: [],
    totalPages: 0,
    refreching: false,
    filter: {
      query: '',
      page: 1,
    },
  },
  isLoading: true,
  movieDetails: { item: {} },
};

describe('Search Movies reducer', () => {
  it('Sould return initial state', () => {
    expect(movieReducer(undefined)).toEqual(INITIAL_STATE);
  });

  it('Sould handle seacrh movies and update state with the new filter', () => {
    expect(
      movieReducer(undefined, {
        payload: { page: 1, query: 'the movie' },
        type: movieActionTypes.MOVIES_GET_REQUEST,
      })
    ).toEqual({
      ...INITIAL_STATE,
      movies: {
        ...INITIAL_STATE.movies,
        filter: {
          query: 'the movie',
          page: 1,
        },
      },
    });
  });

  it('Sould handle seacrh movies success', () => {
    const payload = {
      page: 1,
      total_results: 3,
      total_pages: 1,
      results: [
        {
          poster_path: '/rdQrh59Gq0DX8OmoQeECUtBkoH5.jpg',
          id: 169822,
          title: 'Fast and Furious',
          overview: 'Joel and Garda Sloan, a .....',
          release_date: '1939-10-06',
        },
        {
          poster_path: '/b9gTJKLdSbwcQRKzmqMq3dMfRwI.jpg',
          id: 82992,
          title: 'Fast & Furious 6',
          overview: 'Hobbs has Dominic and .....',
          release_date: '2013-05-22',
        },
        {
          id: 474368,
          title: 'Insanity - Fast and Furious Abs',
          release_date: '2016-04-13',
          overview: 'Shaun T condenses a 45-minute workout into ....',
          poster_path: '/7Xc1rr5EXzirZuDEx3iDzN3VjXj.jpg',
        },
      ],
    };
    expect(
      movieReducer(INITIAL_STATE, {
        payload,
        type: movieActionTypes.MOVIES_GET_SUCCESS,
      }).movies.list.length
    ).toEqual(3);
    expect(
      movieReducer(INITIAL_STATE, {
        payload,
        type: movieActionTypes.MOVIES_GET_SUCCESS,
      }).movies.totalPages
    ).toBe(1);
    expect(
      movieReducer(INITIAL_STATE, {
        payload,
        type: movieActionTypes.MOVIES_GET_SUCCESS,
      }).isLoading
    ).toBeFalsy();
  });

  it('Sould handle seacrh movies success empty result', () => {
    const payload = {
      page: 1,
      total_results: 0,
      total_pages: 0,
      results: [],
    };
    expect(
      movieReducer(INITIAL_STATE, {
        payload,
        type: movieActionTypes.MOVIES_GET_SUCCESS,
      }).movies.list.length
    ).toEqual(0);
    expect(
      movieReducer(INITIAL_STATE, {
        payload,
        type: movieActionTypes.MOVIES_GET_SUCCESS,
      }).movies.totalPages
    ).toBe(0);
    expect(
      movieReducer(INITIAL_STATE, {
        payload,
        type: movieActionTypes.MOVIES_GET_SUCCESS,
      }).isLoading
    ).toBeFalsy();
  });
});

describe('get Movie by Id', () => {
  it('Sould handle get movie by Id success empty result', () => {
    const payload = {
      backdrop_path: '/hpgda6P9GutvdkDX5MUJ92QG9aj.jpg',
      genres: [{ id: 28, name: 'Action' }],
      id: 384018,
      overview:
        "A spinoff of The Fate of the Furious, focusing on Johnson's US Diplomatic Security Agent Luke Hobbs forming an unlikely alliance with Statham's Deckard Shaw.",
      poster_path: '/keym7MPn1icW1wWfzMnW3HeuzWU.jpg',
      release_date: '2019-08-01',
      title: 'Fast and Furious Presents: Hobbs and Shaw',
      vote_average: 6.5,
      vote_count: 780,
    };

    expect(
      movieReducer(INITIAL_STATE, {
        payload,
        type: movieActionTypes.MOVIE_GET_SUCCESS,
      }).movieDetails.item.title
    ).toBe('Fast and Furious Presents: Hobbs and Shaw');
    expect(
      Array.isArray(
        movieReducer(INITIAL_STATE, {
          payload,
          type: movieActionTypes.MOVIE_GET_SUCCESS,
        }).movieDetails.item.genres
      )
    ).toBeTruthy();
    expect(
      movieReducer(INITIAL_STATE, {
        payload,
        type: movieActionTypes.MOVIES_GET_SUCCESS,
      }).isLoading
    ).toBeFalsy();
  });
});
